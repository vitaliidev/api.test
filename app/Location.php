<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['longitude', 'latitude', 'house_number', 'road', 'city_id', 'region_id'];

    public $timestamps = false;

    public function region()
    {
        return $this->belongsTo('App\Region');
    }
}
