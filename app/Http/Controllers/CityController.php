<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * @param $name
     * @return mixed
     * Перевіряє наявність вказаного міста в БД,
     * якщо не виявлено додає місто в БД
     */
    public static function getCity($name)
    {
        $city = City::where('name', '=', $name)->get();
        if (count($city) == 0) {
            $newCity = City::create([
                'name' => $name,
            ]);
            return $newCity->id;
        } else {
            return $city[0]->id;
        }
    }

}
