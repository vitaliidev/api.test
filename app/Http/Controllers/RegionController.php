<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * @param $name
     * @return mixed
     * Перевіряє наявність вказаного регіону в БД,
     * якщо не виявлено додає регіон в БД
     */
    public static function getRegion($name)
    {
        $region = Region::where('name', '=', $name)->get();

        if (count($region) == 0) {
            $newRegion = Region::create([
                'name' => $name,
            ]);
            return $newRegion->id;
        } else {
            return $region[0]->id;
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * Повертає список усіх регіонів
     */
    public function getListRegions()
    {
        $regions = Region::all();
        return response()->json($regions);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Повертає список адрес за вказаним регіоном (параметр id)
     */
    public function getAddressesByRegions($id)
    {
        $addresses = Region::find($id)->locations()->get();
        return response()->json($addresses);
    }
}
