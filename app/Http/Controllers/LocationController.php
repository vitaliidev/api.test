<?php

namespace App\Http\Controllers;

use App\Location;
use App\Region;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Записує координати та дані, що відповідають
     * координатам в БД
     */
    public function getAddress(Request $request)
    {
        $longitude = $request->longitude;
        $latitude = $request->latitude;
        settype($longitude, "string");
        settype($latitude, "string");

        $url = "https://eu1.locationiq.com/v1/reverse.php?key=08a4bc77e589d0&lat=$longitude&lon=$latitude&format=json";
        $data = @file_get_contents($url);
        $jsondata = json_decode($data, true);

        if (is_array($jsondata)) {
            $houseNumber = isset($jsondata['address']['house_number']) ? $jsondata['address']['house_number'] : null;
            $road = isset($jsondata['address']['road']) ? $jsondata['address']['road'] : null;
            $cityName = isset($jsondata['address']['city']) ? explode(' ', $jsondata['address']['city'])[0] : $jsondata['address']['village'];
            $regionName = isset($jsondata['address']['state']) ? $jsondata['address']['state'] : $jsondata['address']['city'];
            $city_id = CityController::getCity($cityName);
            $region_id = RegionController::getRegion($regionName);

            $response = Location::create([
                'longitude' => $longitude,
                'latitude' => $latitude,
                'house_number'=> $houseNumber,
                'road' => $road,
                'city_id' => $city_id,
                'region_id' => $region_id,
            ]);

            $message = 'Success!';
        } else {
            $message = 'Error!';
            $response = 'Repeat your request.';
        }

        return response()->json(['Message' => $message, 'Response' => $response]);
    }
}
