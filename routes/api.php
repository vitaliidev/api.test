<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Задання координат
Route::post('/location', 'LocationController@getAddress');

// Отримання списку регіонів
Route::get('/region', 'RegionController@getListRegions');

// Отримання списку адрес за вказаним регіоном (параметр id)
Route::get('/address/{id}', 'RegionController@getAddressesByRegions');