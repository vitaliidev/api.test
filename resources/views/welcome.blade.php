<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <h3>Список доступних методів</h3>
        <ul class="list-group">
            <li class="list-group-item">
                POST /location - метод для запису геокоординат на сервер. Необхідні параметри: $longitude, $latitude.
            </li>
            <li class="list-group-item">
                GET /region- метод для отримання списку всіх регіонів в форматі JSON.
            </li>
            <li class="list-group-item">
                GET /address/{id}- метод для отримання списку адрес за вказаним регіоном в форматі JSON. Необхідний параметр $id регіону.
            </li>
        </ul>
    </div>
    </body>
</html>
